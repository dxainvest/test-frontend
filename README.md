# Test Frontend

Aqui na DXA, aplicamos este mesmo teste para as vagas em todos os níveis, ou seja, um candidato a uma vaga de frontend júnior fará o mesmo teste de um outro candidato a uma vaga de frontend sênior, mudando obviamente o nosso critério de avaliação do resultado do teste.

## Instruções

Você deverá criar um <i>fork</i> deste projeto, e desenvolver em cima do seu fork. Use o README principal do seu repositório para nos contar como foi resolver seu teste, as decisões tomadas, como você organizou e separou seu código, e principalmente as instruções de como rodar seu projeto, afinal a primeira pessoa que irá rodar seu projeto será um programador da nossa equipe, e se você conseguir explicar para ele como fazer isso, você já começou bem!

Lembre-se que este é um teste técnico e não existe apenas uma resposta correta.

## O Desafio

Desenvolver (parcial ou completamente) a aplicação de frontend no framework preferido que represente o protótipo abaixo.

- [Figma](https://www.figma.com/file/3WVN7Jno5YNVigLLYjtvSe/Teste-DXA---FrontEnd?node-id=0%3A1)

## O que nós observaremos no seu teste

- [ ] Ver na solução a utilização de um framework da sua escolha, mas por aqui utilizamos o VueJS. Utilize o framework da melhor forma possível
- [ ] Também ver a utilização de dependency managers (npm, webpack)
- [ ] Um HTML escrito da maneira mais semântica possível 
- [ ] CSS3/4 - Com um pre processador de CSS
- [ ] Mobile first e layout responsivo
- [ ] Componentização e extensibilidade dos componentes Javascript
- [ ] Organização, semântica, estrutura, legibilidade, manutenibilidade do seu código
- [ ] Commits parciais, demonstrando a sua forma de pensar e estruturar a solução

## O que nos impressionaria

- [ ] Ver a solução rodando online (Azure Web, Heroku)
